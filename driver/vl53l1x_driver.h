/**
 * High level interface to acquire range data from 
 * Sparkfun's VL53L1X ToF range finder, using the 
 * Qwiic Mux Breakout (TCA9548A).  
 * 
 * Author: Lucas Waelti (LucasWaelti), December 2020
 */

#pragma once 

#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>
#include <stdio.h>

#define BUS                 7       // I2C bus 
#define BAUDRATE            100000  // Bit rate  
#define VL53L1X_I2C_ADDR    0x29    // Device default address 
#define MUX_I2C_ADDR        0x70    // Qwiic multiplexer (TCA9548A) address 
#define REG_ADDR_NUM_BITS   16      // number of bits to address registers 

typedef unsigned short uint16_t;

/**
 * Set some communication parameters required for the VL53L1X sensor
 * @param baudrate:         bit rate in Hz
 * @param reg_addr_size:    number of bits for register addresses
 * @param swap:             whether to swap bytes of 16 bits register addresses. If 0, VOXL sends LSB first. 
 */
void set_com_param(int baudrate, int reg_addr_size, int swap);

/**
 * Verifiy the device ID (must be 0xEEAC)
 * @return (0) is ID correct, (-1) otherwise 
 */
int check_sensor_id();

/**
 * Enable the sensor and start measuring 
 */
int enable_sensor();

/**
 * Returns the distance in mm. 
 */
uint16_t get_distance();

/**
 * Stop measuring 
 */
int disable_sensor();

/**
 * Enable a channel to make the device visible
 * [0:7]:   enable the specified channel 
 * [-1]:    disable all
 * [8]:     enable all (should not be used)
 * 
 * Note that enabling a channel disables the others. 
 */
int mux_enable_channel(int channel);

#ifdef __cplusplus
}
#endif