This directory contains the executables and libraries to interface 
the Sparkfun VL53LX1 sensor.  

## voxl_io
The `libvoxl_io.so` can be obtained by compiling the [voxl_io](https://gitlab.com/voxl-public/core-libs/libvoxl_io) package (follow the instructions of the readme). 

The `voxl_io.h` file may need the directive `#include <stdlib.h>` to be added. 