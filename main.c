#include "VL53L1X_api.h"
#include "vl53l1x_driver.h"
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>

#define BUS                 7       // I2C bus 
#define BAUDRATE            100000  // Bit rate  
#define VL53L1X_I2C_ADDR    0x29    // Device default address 
#define MUX_I2C_ADDR        0x70    // Qwiic multiplexer (TCA9548A) address 
#define REG_ADDR_NUM_BITS   16      // number of bits to address registers 

static volatile uint8_t keepRunning = 1;
void intHandler(int dummy) {
    keepRunning = 0;
}

/**
 * read some calibration data from on-board pressure sensor BMP-280
 */
void test_pressure_sensor(){
    
    printf("Testing pressure sensor\n"); 

    uint32_t i2c_bus = 3; 
    uint32_t i2c_bit_rate = 400000; 
    uint32_t i2c_address = 0x76;
    uint32_t start_register = 0x88;
    uint32_t read_size = 10;
    uint8_t read_data[10];

    // Set communicaton parameters 
    set_baudrate(i2c_bit_rate); 
    set_register_address_size(8); 
    swap_reg_addr_bytes(0); 
    open_bus(i2c_bus);

    read_registers(i2c_bus,i2c_bit_rate,i2c_address,start_register,read_data,read_size); 

    printf("Measurement on bus 3: ");
    for(int i=0; i<10; i++){
        printf("%d ",read_data[i]);
    }printf("\n");

    close_bus(3); 

    printf("Done\n");
}

/**
 * Run a VL53L1X on i2c port 7 with default address 0x29 
 */
void test_vl53l1x(){

    // Handle Ctrl-C keyboard interrupt
    signal(SIGINT, intHandler);
    
    printf("Running VL53L1X driver on bus %d, address 0x%X\n", BUS, VL53L1X_I2C_ADDR);

    uint16_t dev = VL53L1X_I2C_ADDR;    // device's address 
    VL53L1X_ERROR status = 0;   // I2C operations status 

    // Set communicaton parameters 
    set_baudrate(BAUDRATE); 
    set_register_address_size(REG_ADDR_NUM_BITS); 
    swap_reg_addr_bytes(1); // Sparkfun needs MSB first

    // Read sensor ID, should be 0xEEAC
    uint8_t read_data[2];
    read_registers(BUS,BAUDRATE,VL53L1X_I2C_ADDR,VL53L1_IDENTIFICATION__MODEL_ID,read_data,2); 
    printf("Sensor ID: 0x%X%X (must be 0xEEAC)\n", read_data[0],read_data[1]);

    // Boot 
    printf("Waiting for boot...\n");
    uint8_t booted = 0;
    for(int i=0; i<10 && !booted; i++){
        if(VL53L1X_BootState(dev, &booted)){
            printf("Error: BootState() returned status -1\n"); 
            goto close; 
        }
        //printf("Device has not booted\n");
        VL53L1_WaitMs(dev,100); 
    }
    if(!booted){
        printf("Device on address 0x%X is not booted, aborting\n",dev);
        goto close;
    }

    // Init
    printf("Sensor booted, initializing...\n");
    if(VL53L1X_SensorInit(dev))
        goto close; 

    // Get distance measurement 
    printf("Measuring range...\n");
    VL53L1X_StartRanging(dev);
    uint8_t isDataReady = 0; 
    while(keepRunning){
        while(isDataReady == 0)
            VL53L1X_CheckForDataReady(dev, &isDataReady);
        VL53L1X_ClearInterrupt(dev);
        uint16_t distance;
        VL53L1X_GetDistance(dev, &distance);
        printf("Measured %d mm\t\r",distance);
    }printf("\n");

close:
    // Shutdown 
    printf("Shutting down...\n");
    VL53L1X_ClearInterrupt(dev);
    VL53L1X_StopRanging(dev);   

    printf("Done\n"); 
}

/**
 * Lists currently enabled channels - DOES NOT WORK 
 */
/*void mux_print_open_channels(){

    uint8_t data; 
    swap_reg_addr_bytes(0); // register address is 0, no swap required
    read_registers(BUS,BAUDRATE,MUX_I2C_ADDR,0,&data,1);
    printf("Raw channel state: %d\n",data); 

    if(!data)
        printf("No channel currently opened.\n");
    else{
        int n_open = 0; 
        printf("Following channels are opened:\n");
        for(uint8_t i=0; i<8; i++){
            if(data & (0x1 << i)){
                printf("%d ",i);
                n_open++; 
            }
        }printf(" (%d channels)\n",n_open);
    }
}*/

/**
 * Test multiple sensors connected to the multiplexer 
 */
void test_api(){

    // Channels where a VL53L1X is connected 
    int n_channels = 2;         // 1
    int channels[2] = {1,5};    // {-1}

    set_com_param(BAUDRATE,REG_ADDR_NUM_BITS,1); 

    // Check sensor ids 
    for(int i=0; i<n_channels; i++){
        if(!mux_enable_channel(channels[i]))
            printf("Channel %d ID status: %d\n", channels[i], check_sensor_id());
    }

    // Enable all connected sensors 
    for(int i=0; i<n_channels; i++){
        if(mux_enable_channel(channels[i]))
            printf("FATAL ERROR\n");
        enable_sensor();
    }

    // Get the distance once per sensor
    for(int s=0; s<10; s++)
        for(int i=0; i<n_channels; i++){
            mux_enable_channel(channels[i]);
            printf("Channel %d, distance: %d mm\n", channels[i], get_distance());
        }

    // Disable all sensors 
    for(int i=0; i<n_channels; i++){
        mux_enable_channel(channels[i]);
        disable_sensor(); 
    }
}

int main(int argc, char* argv[]){

    open_bus(BUS); 

    //scan_i2c_bus(BUS,0);

    //test_pressure_sensor(); exit(EXIT_SUCCESS);

    //mux_enable_channel(1); // enable if connected to mux 
    //test_vl53l1x();

    test_api(); 

    close_bus(BUS); 

    return 0; 
}