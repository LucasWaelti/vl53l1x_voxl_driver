#!/bin/bash 

function build_driver {
	mkdir -p build
	cd build
	cmake -Wdev .. -DCMAKE_TOOLCHAIN_FILE=${APPS_TOOLCHAIN_FILE}
	make
	cd -
}

set -e # exit on error

APPS_TOOLCHAIN_FILE=${CMAKE_HEXAGON_DIR}/toolchain/Toolchain-arm-linux-gnueabi.cmake

build_driver

mv build/driver_debug driver/driver_debug 
mv build/libvl53l1x_driver.so driver/libvl53l1x_driver.so 

echo ""
echo "DONE"