#!/bin/echo DO NOT USE (no cross-compilation support)
# Set compiler
CC = gcc

# Include headers 
INC_HDR := -I. -IAPI/core -IAPI/platform -Ilib

# Define final executable 
EXE := vl53l1x_driver 

# List source
SRC := $(wildcard API/core/*.c) \
		$(wildcard API/platform/*.c) \
		main.c 

# List headers
HDR := $(wildcard API/core/*.h) \
		$(wildcard API/platform/*.h) 

# Derive objects
OBJ := $(patsubst %.c, %.o, $(SRC))

# Set flags 
CPPFLAGS := $(INC_HDR)		# -I is a preprocessor flag, not a compiler flag -MMD -MP
CFLAGS   := -Wall         	# some warnings about bad code
LDFLAGS  := -L./lib       # -L is a linker flag
LDLIBS   := -lm           # Left empty if no libs are needed

.PHONY: all clean 

all: $(EXE)

$(EXE): $(OBJ) 
	@echo Linking $@
	$(CC) $(CPPFLAGS) $(CFLAGS) $(LDFLAGS) $(LDLIBS) $^ -o $@ 

%.o: %.c $(HDR)
	@echo Building $@ 
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@

clean:
	@find . -type f -name '*.o' -delete
	@find . -type f -name '*.d' -delete
	@rm -f $(EXE) 