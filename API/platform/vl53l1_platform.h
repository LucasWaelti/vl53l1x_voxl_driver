/**
 * @file  vl53l1_platform.h
 * @brief Those platform functions are platform dependent and have to be implemented by the user
 */
 
#ifndef _VL53L1_PLATFORM_H_
#define _VL53L1_PLATFORM_H_

#include "vl53l1_types.h"
#include "voxl_io.h"

/**
 * Define communication's bit rate (Hz, e.g. 100000, 400000)
 */
void set_baudrate(int baudrate);

/**
 * Define the registers address size in bits
 */
void set_register_address_size(int size); 

/**
 * Enable (1) or disable (0) register address bytes swap (2 bytes address only)
 * VOXL sends the LSB first but most implementations expect the MSB first. 
 */
void swap_reg_addr_bytes(uint8_t swap);

/**
 * Perform a byte swap for 16 bits values. Should not be used directly, use the api instead. 
 */
uint16_t _swap_bytes(uint16_t r);

/**
 * Open the specified bus. 
 * Bus definition available here: https://docs.modalai.com/voxl-i2c-io/#i2c-port-numbers 
 */
int open_bus(uint32_t i2c_bus);

/**
 * Close the specified bus. 
 * Bus definition available here: https://docs.modalai.com/voxl-i2c-io/#i2c-port-numbers 
 */
int close_bus(uint32_t i2c_bus);

/**
 * Print out all connected devices on the specified bus. 
 */
int scan_i2c_bus(uint32_t i2c_bus, uint32_t reg_to_read);

int read_registers(uint32_t i2c_bus, uint32_t i2c_bit_rate, uint32_t i2c_address, uint32_t start_register, uint8_t * read_data, uint32_t read_size);
int write_registers(uint32_t i2c_bus, uint32_t i2c_bit_rate, uint32_t i2c_address, uint32_t start_register, uint8_t * write_data, uint32_t write_size);

/////////////////// BASE API FUNCTIONS ///////////////////

#ifdef __cplusplus
extern "C"
{
#endif

typedef struct {
	uint32_t dummy;
} VL53L1_Dev_t;

typedef VL53L1_Dev_t *VL53L1_DEV;

/** @brief VL53L1_WriteMulti() definition.\n
 * To be implemented by the developer
 */
int8_t VL53L1_WriteMulti(
		uint16_t 			dev,
		uint16_t      index,
		uint8_t      *pdata,
		uint32_t      count);
/** @brief VL53L1_ReadMulti() definition.\n
 * To be implemented by the developer
 */
int8_t VL53L1_ReadMulti(
		uint16_t 			dev,
		uint16_t      index,
		uint8_t      *pdata,
		uint32_t      count);
/** @brief VL53L1_WrByte() definition.\n
 * To be implemented by the developer
 */
int8_t VL53L1_WrByte(
		uint16_t dev,
		uint16_t      index,
		uint8_t       data);
/** @brief VL53L1_WrWord() definition.\n
 * To be implemented by the developer
 */
int8_t VL53L1_WrWord(
		uint16_t dev,
		uint16_t      index,
		uint16_t      data);
/** @brief VL53L1_WrDWord() definition.\n
 * To be implemented by the developer
 */
int8_t VL53L1_WrDWord(
		uint16_t dev,
		uint16_t      index,
		uint32_t      data);
/** @brief VL53L1_RdByte() definition.\n
 * To be implemented by the developer
 */
int8_t VL53L1_RdByte(
		uint16_t dev,
		uint16_t      index,
		uint8_t      *pdata);
/** @brief VL53L1_RdWord() definition.\n
 * To be implemented by the developer
 */
int8_t VL53L1_RdWord(
		uint16_t dev,
		uint16_t      index,
		uint16_t     *pdata);
/** @brief VL53L1_RdDWord() definition.\n
 * To be implemented by the developer
 */
int8_t VL53L1_RdDWord(
		uint16_t dev,
		uint16_t      index,
		uint32_t     *pdata);
/** @brief VL53L1_WaitMs() definition.\n
 * To be implemented by the developer
 */
int8_t VL53L1_WaitMs(
		uint16_t dev,
		int32_t       wait_ms);

/////////////////// BASE API FUNCTIONS (END) ///////////////////

#ifdef __cplusplus
}
#endif

#endif
