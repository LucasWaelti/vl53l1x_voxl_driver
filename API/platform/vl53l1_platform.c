
/* 
* This file is part of VL53L1 Platform 
* 
* Copyright (c) 2016, STMicroelectronics - All Rights Reserved 
* 
* License terms: BSD 3-clause "New" or "Revised" License. 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are met: 
* 
* 1. Redistributions of source code must retain the above copyright notice, this 
* list of conditions and the following disclaimer. 
* 
* 2. Redistributions in binary form must reproduce the above copyright notice, 
* this list of conditions and the following disclaimer in the documentation 
* and/or other materials provided with the distribution. 
* 
* 3. Neither the name of the copyright holder nor the names of its contributors 
* may be used to endorse or promote products derived from this software 
* without specific prior written permission. 
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE 
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
* 
*/

#include "vl53l1_platform.h"
#include <string.h>
#include <time.h>
#include <math.h>
#include <errno.h>

// parameters 
static uint p_bit_rate = 400000; 
static uint32_t p_reg_addr_size = 8; 
static uint p_bus = 7; 
static uint8_t swap_enabled = 0; 

// debug 
static uint8_t verbose = 0;

void set_baudrate(int baudrate){
	p_bit_rate = baudrate; 
}

void set_register_address_size(int size){
    p_reg_addr_size = size; 
}

void swap_reg_addr_bytes(uint8_t swap){
    if(swap && p_reg_addr_size != 16){
        fprintf(stderr, "Attempting to swap register address bytes, but address has size %d (should be 16 bits)\n", p_reg_addr_size);
        swap_enabled = 0; 
    }
    else 
        swap_enabled = swap; 
}

uint16_t _swap_bytes(uint16_t r){
    uint16_t swap; 
    swap = r >> 8;  // MSB -> LSB
    swap |= r << 8; // LSB -> MSB
    return swap; 
}

int open_bus(uint32_t i2c_bus){
    if (voxl_i2c_init(i2c_bus)){
        fprintf(stderr, "ERROR initializing i2c bus %d\n",i2c_bus);
        return -1;
    }
	p_bus = i2c_bus; 
    return 0; 
}

int close_bus(uint32_t i2c_bus){
    if (voxl_i2c_close(i2c_bus)){
        fprintf(stderr, "ERROR closing i2c bus %d\n",i2c_bus);
        return -1;
    }
	p_bus = i2c_bus;
    return 0; 
}

int scan_i2c_bus(uint32_t i2c_bus, uint32_t reg_to_read)
{
    if(swap_enabled) reg_to_read = _swap_bytes(reg_to_read); 
    
    printf("Scanning I2C Bus %d, bit rate %d, read address %d (0x%02X)\n",i2c_bus,p_bit_rate,reg_to_read,reg_to_read);

    // allocate shared memory buffer for reading data
    uint32_t read_data_buffer_size = 1;
    uint8_t* read_data_buffer = voxl_rpc_shared_mem_alloc(read_data_buffer_size);
    if(read_data_buffer==NULL){
        fprintf(stderr, "failed to allocate shared rpc memory\n");
        return -1;
    }

    uint32_t test_addr;
    uint32_t test_register_addr = reg_to_read;
    uint32_t i2c_bit_rate_hz    = p_bit_rate;
    uint32_t i2c_timeout_us     = 1000;
    uint32_t found_devices      = 0;

    for (test_addr=1; test_addr<127; test_addr++){
        //printf("Testing I2C slave address %d...\n",test_addr);
        if (voxl_i2c_slave_config(i2c_bus, test_addr, i2c_bit_rate_hz, i2c_timeout_us)){
            fprintf(stderr, "failed to set i2c slave config on bus %d, address %d, bit rate %d, timeout %dus\n",
                i2c_bus, test_addr,i2c_bit_rate_hz,i2c_timeout_us);
            break;
        }

        if (voxl_i2c_read(i2c_bus, test_register_addr, p_reg_addr_size, read_data_buffer, 1) == 0){
            printf("Found I2C device with slave address %d (0x%02X). Register %d (0x%02X) is %d (0x%02X)\n",
                test_addr,test_addr,reg_to_read,reg_to_read,read_data_buffer[0],read_data_buffer[0]);
            found_devices++;
        }
    }

    printf("Found %d devices\n",found_devices);

    // cleanup shared memory
    voxl_rpc_shared_mem_free(read_data_buffer);
    voxl_rpc_shared_mem_deinit();

    return 0;
}

int read_registers(uint32_t i2c_bus, uint32_t i2c_bit_rate, uint32_t i2c_address, uint32_t start_register, uint8_t * read_data, uint32_t read_size)
{   
    if(swap_enabled) start_register = _swap_bytes(start_register); 
    
    if(verbose) printf("[read_register] address 0x%02X, register 0x%X, nbytes %d\n", i2c_address, start_register, read_size);

    // allocate shared memory buffer for reading data
    //int64_t t_mem_alloc_start = voxl_apps_time_realtime_ns();
    uint32_t read_data_buffer_size = read_size;
    uint8_t* read_data_buffer = voxl_rpc_shared_mem_alloc(read_data_buffer_size);
    if(read_data_buffer==NULL){
        fprintf(stderr, "failed to allocate shared rpc memory\n");
        return -1;
    }

    uint32_t i2c_timeout_us = 1000;

    if (voxl_i2c_slave_config(i2c_bus, i2c_address, i2c_bit_rate, i2c_timeout_us)){
        fprintf(stderr, "failed to set i2c slave config on bus %d, address %d, bit rate %d, timeout %dus\n",i2c_bus, i2c_address,i2c_bit_rate,i2c_timeout_us);
        return -1;
    }

    if (voxl_i2c_read(i2c_bus, start_register, p_reg_addr_size, read_data_buffer, read_size)){  
        fprintf(stderr, "failed to read %d bytes from i2c_address %d @ %d\n",read_size, i2c_address, start_register);
        return -1;
    }
    else {
        if(read_size == 1){
            *read_data = read_data_buffer[0];
            if(verbose) printf("[read_register] read value: %d(0x%02X)\n",*read_data,*read_data);
        }
        else{
            uint32_t ii;
            if(verbose) printf("[read_register] read value: ");
            for (ii=0; ii<read_size; ii++){
                read_data[ii] = read_data_buffer[ii];
                if(verbose) printf("%d(0x%02X) ",read_data_buffer[ii],read_data_buffer[ii]);
            }
            if(verbose) printf("\n");
        }
    }

    // cleanup shared memory
    voxl_rpc_shared_mem_free(read_data_buffer);
    voxl_rpc_shared_mem_deinit();

    return 0;
}

int write_registers(uint32_t i2c_bus, uint32_t i2c_bit_rate, uint32_t i2c_address, uint32_t start_register, uint8_t * write_data, uint32_t write_size)
{
    if(swap_enabled) start_register = _swap_bytes(start_register); 

    uint32_t i2c_timeout_us     = 1000;

    if (voxl_i2c_slave_config(i2c_bus, i2c_address, i2c_bit_rate, i2c_timeout_us)){
        fprintf(stderr, "failed to set i2c slave config on bus %d, address %d, bit rate %d, timeout %dus\n",i2c_bus, i2c_address,i2c_bit_rate,i2c_timeout_us);
        return -1;
    }

    if (voxl_i2c_write(i2c_bus, start_register, p_reg_addr_size, write_data, write_size)){
        fprintf(stderr, "failed to write %d bytes to i2c_address %d @ %d\n",write_size, i2c_address, start_register);
        return -1;
    }

    return 0;
}

/////////////////// BASE API FUNCTIONS ///////////////////

int8_t VL53L1_WriteMulti( uint16_t dev, uint16_t index, uint8_t *pdata, uint32_t count) {
	if(write_registers(p_bus,p_bit_rate,dev,index,pdata,count))
		return -1; 
	return 0; 
}

int8_t VL53L1_ReadMulti(uint16_t dev, uint16_t index, uint8_t *pdata, uint32_t count){
	if(read_registers(p_bus,p_bit_rate,dev,index,pdata,count))
		return -1; 
	return 0; 
}

int8_t VL53L1_WrByte(uint16_t dev, uint16_t index, uint8_t data) {
	if(write_registers(p_bus,p_bit_rate,dev,index,&data,1))
		return -1;
	return 0; 
}

int8_t VL53L1_WrWord(uint16_t dev, uint16_t index, uint16_t data) {
	
    // Split word into array (MSB first)
    uint8_t d[2]; 
    d[0] = data >> 8; 
    d[1] = data & 0xFF; 

    // Write the data 
    if(write_registers(p_bus,p_bit_rate,dev,index,d,2))
		return -1;
	return 0; 
}

int8_t VL53L1_WrDWord(uint16_t dev, uint16_t index, uint32_t data) {

    // Split word into array (MSB first)
    uint8_t d[4]; 
    d[0] = data >> 24; 
    d[1] = (data >> 16) & 0xFF; 
    d[2] = (data >> 8 ) & 0xFF; 
    d[3] = data & 0xFF; 

    // Write the data 
    if(write_registers(p_bus,p_bit_rate,dev,index,d,2))
		return -1;
	return 0; 
}

int8_t VL53L1_RdByte(uint16_t dev, uint16_t index, uint8_t *data) {
	if(read_registers(p_bus,p_bit_rate,dev,index,data,1))
		return -1; 
	return 0; 
}

int8_t VL53L1_RdWord(uint16_t dev, uint16_t index, uint16_t *data) {

    // Read the data into array 
    uint8_t d[2];
    if(read_registers(p_bus,p_bit_rate,dev,index,d,2))
		return -1;

    // Combine array into single value (MSB first)
	*data = 0x0000; 
	*data |= d[0] << 8; 
    *data |= d[1];  
	return 0; 
}

int8_t VL53L1_RdDWord(uint16_t dev, uint16_t index, uint32_t *data) {
	
    // Read the data into array 
    uint8_t d[4];
    if(read_registers(p_bus,p_bit_rate,dev,index,d,2))
		return -1;

    // Combine array into single value (MSB first)
	*data = 0x00000000; 
	*data |= d[0] << 24; 
    *data |= d[1] << 16; 
    *data |= d[2] << 8;
    *data |= d[0];  
	return 0; 
}

int msleep(long msec);
int8_t VL53L1_WaitMs(uint16_t dev, int32_t wait_ms){
	// TODO: this is probably not what is expected... 
	// Need to make the device wait but function does 
	// not seems to be used
	if(msleep(wait_ms))
		return -1; 
	return 0; 
}

/////////////////// BASE API FUNCTIONS (END) ///////////////////    

/**
 * Sleep for the requested number of milliseconds. 
 */
int msleep(long msec)
{
    struct timespec ts;
    int res;

    if (msec < 0)
    {
        errno = EINVAL;
        return -1;
    }

    ts.tv_sec = msec / 1000;
    ts.tv_nsec = (msec % 1000) * 1000000;

    do {
        res = nanosleep(&ts, &ts);
    } while (res && errno == EINTR);

    return res;
}