/**
 * High level interface to acquire range data from 
 * Sparkfun's VL53L1X ToF range finder. 
 * 
 * Author: Lucas Waelti (LucasWaelti), December 2020
 */

#include "vl53l1x_driver.h"
#include "VL53L1X_api.h"

void set_com_param(int baudrate, int reg_addr_size, int swap){

    set_baudrate(baudrate); 
    set_register_address_size(reg_addr_size); 

    // Sparkfun needs MSB first for 16 bits register addresses
    swap_reg_addr_bytes(swap); 
}

int check_sensor_id(){
    
    uint8_t read_data[2];
    read_registers(BUS,BAUDRATE,VL53L1X_I2C_ADDR,VL53L1_IDENTIFICATION__MODEL_ID,read_data,2); 

    uint16_t id = 0x0000; 
    id |= read_data[0] << 8; 
    id |= read_data[1]; 

    if(id != 0xEACC) // should be EEAC ?
        return -1;
    else 
        return 0; 
}

int enable_sensor(){
    
    // Boot 
    uint8_t booted = 0;
    for(int i=0; i<10 && !booted; i++){
        if(VL53L1X_BootState(VL53L1X_I2C_ADDR, &booted)){
            printf("Error: BootState() returned status -1\n"); 
            return -1; 
        }
        VL53L1_WaitMs(VL53L1X_I2C_ADDR,100); 
    }
    if(!booted){
        printf("Error: device on address 0x%X is not booted, aborting\n",VL53L1X_I2C_ADDR);
        return -1; 
    }

    // Init
    if(VL53L1X_SensorInit(VL53L1X_I2C_ADDR)){
        printf("Error: sensor not initialized\n");
        return -1; 
    }

    // Start range measurements  
    if(VL53L1X_StartRanging(VL53L1X_I2C_ADDR)){
        printf("Error: could not start measuring range\n");
        return -1; 
    }
    return 0; 
}

uint16_t get_distance(){
    uint8_t isDataReady = 0; 
    while(isDataReady == 0)
        VL53L1X_CheckForDataReady(VL53L1X_I2C_ADDR, &isDataReady);
    VL53L1X_ClearInterrupt(VL53L1X_I2C_ADDR);
    uint16_t distance;
    VL53L1X_GetDistance(VL53L1X_I2C_ADDR, &distance);
    return distance; 
}

int disable_sensor(){
    VL53L1X_ClearInterrupt(VL53L1X_I2C_ADDR);
    VL53L1X_StopRanging(VL53L1X_I2C_ADDR);
    return 0;  
}

int mux_enable_channel(int channel){

    uint8_t data = 0x00; 
    
    // Enable a channel
    if(channel >= 0 && channel < 8){
        data = 0x1 << channel; 
    }
    // Disable all channels
    else if(channel == -1){
        data = 0x00;
    }
    // Enable all channels
    else if(channel == 8){
        data = 0xFF; 
    }
    else{
        printf("[mux_enable_channel]: error, invalid channel, aborting");
        return -1; 
    }

    // Apply action 
    if(write_registers(BUS,BAUDRATE,MUX_I2C_ADDR,0x0000,&data,1))
        return -1; 
    
    return 0; 
}