# VL53L1X_voxl_driver

This driver implementation is based on the [API](https://www.st.com/en/embedded-software/stsw-img009.html#) provided by [ST](https://www.st.com/content/st_com/en.html). It allows to use Sparkfun's VL53L1X ToF range finder with ModalAI's VOXL board. 

Look also at the readme in the `driver/` folder. 

## Building the code

The project has to be built from within the [hexagon docker](https://docs.modalai.com/install-voxl-docker/#optional-install-the-voxl-hexagon-docker-image). The `build.sh` and `clean.sh` scripts can be used and will generate the executables in the `driver/` directory. 

## Pushing onto voxl

The `driver/` directory contains all generated executables and libraries and can therefore be pushed on VOXL. To do so, the following command can be used: 
```bash
adb push ./driver /home/root
```

<!--
## I2C interactions

### Boot example

To check whether the device has booted, the host interrogates the register `VL53L1_FIRMWARE__SYSTEM_STATUS = 0x00E5` by calling (in file `VL53L1X_api.c`): 
```cpp
/**
 * This function returns the boot state of the device (1:booted, 0:not booted)
 */
VL53L1X_ERROR VL53L1X_BootState(uint16_t dev, uint8_t *state)
{
	VL53L1X_ERROR status = 0;
	uint8_t tmp = 0;

	status = VL53L1_RdByte(dev,VL53L1_FIRMWARE__SYSTEM_STATUS, &tmp);
	*state = tmp;
	return status;
}
```

In the python driver [`qwiic_vl53l1x.py`](https://github.com/sparkfun/Qwiic_VL53L1X_Py/blob/master/qwiic_vl53l1x.py), the equivalent function is: 
```python
def boot_state(self):
    """
    This function returns the boot state of the device (1:booted, 0:not booted)

    :return: 	* 1- booted
                * 0- not booted
    :rtype:		Integer
    """
    self.status = 0
    state = 0

    state = self.__i2cRead(self.address,VL53L1_FIRMWARE__SYSTEM_STATUS, 1)

    return state
```
The `__i2cRead()` function is as follows: 
```python
def __i2cRead(self, address, register, nbytes):
		"""
		A wrapper for the I2C driver since device needs 16-bit register addresses. 
        Formats register and data values so that they can be written to device as a block for proper I2C transactions.

		:param	register:	16-bit register address
							(can be 8-bit, just writes 0x00 byte prior to value)
		:param	nbytes:		number of bytes in data (*to be read*)
							(needs to be specified for transaction)
		
		:return:	data
		:rtype:		integer
		"""
		
		data = 0
		registerMSB = register >> 8
		registerLSB = register & 0xFF

		if nbytes not in [1, 2, 4]:
			if self.debug == 1:
				print("in __i2cWriteBlock, nbytes entered invalid")
			return

		read_data = self._i2c.__i2c_rdwr__(address, [registerMSB, registerLSB], nbytes)
		buffer = list(read_data)

		for i in range(0, nbytes):
			data = ( buffer[ (nbytes - 1) - i ] << (i*8) ) + data

		return data
```
and the [`__i2c_rdwr__()`](https://github.com/sparkfun/Qwiic_I2C_Py/blob/f4dba4805319a8fc23864f594de4ebe17d0b6e1b/qwiic_i2c/linux_i2c.py#L270) function from the [Qwiic_I2C_Py](https://github.com/sparkfun/Qwiic_I2C_Py) package is: 
```python
def __i2c_rdwr__(self, address, write_message, read_nbytes):
		"""
		Custom method used for 16-bit (or greater) register reads
		:param address: 7-bit address
		:param write_message: list with register(s) to read
		:param read_nbytes: number of bytes to be read

		:return: response of read transaction
		:rtype: list
		"""
		global _i2c_msg
		
		# Loads i2c_msg if not previously loaded
		if _i2c_msg == None:
			from smbus2 import i2c_msg
			_i2c_msg = i2c_msg
        
		# Sets up write and read transactions for reading a register
		write = _i2c_msg.write(address, write_message)
		read = _i2c_msg.read(address, read_nbytes)

		# Read Register
		for i in range(_retry_count):
			try:
				self.i2cbus.i2c_rdwr(write, read)
			
				break # break if try succeeds

			except IOError as ioErr:
				# we had an error - let's try again
				if i == _retry_count-1:
					raise ioErr
				pass
		
		# Return read transaction (list)
		return read
```

Now, if one follows the sequence of calls, the following process happens when checking the boot state: 
- The 16 bits register `VL53L1_FIRMWARE__SYSTEM_STATUS = 0x00E5` is sent to be read at the given address (`0x29`)
- `__i2cRead()` splits the register into two bytes (MSB and LSB)
- `__i2c_rdwr__()` receives the __address__, __[registerMSB, registerLSB]__ and the __number of bytes__ to read there. 
    - The `self.i2cbus.i2c_rdwr()` combines a series of i2c read and write operations in a single transaction (with repeated start bits but no stop bits in between).

The registers therefore have 16 bits addresses. 


```cpp
uint8_t _I2CBuffer[256];

int _I2CWrite(uint16_t Dev, uint8_t *pdata, uint32_t count) {
    int status;
    int i2c_time_out = I2C_TIME_OUT_BASE+ count* I2C_TIME_OUT_BYTE;

    status = HAL_I2C_Master_Transmit(&XNUCLEO53L1A1_hi2c, Dev, pdata, count, i2c_time_out);
    if (status) {
        //VL6180x_ErrLog("I2C error 0x%x %d len", dev->I2cAddr, len);
        //XNUCLEO6180XA1_I2C1_Init(&hi2c1);
    }
    return status;
}

int _I2CRead(uint16_t Dev, uint8_t *pdata, uint32_t count) {
    int status;
    int i2c_time_out = I2C_TIME_OUT_BASE+ count* I2C_TIME_OUT_BYTE;

    status = HAL_I2C_Master_Receive(&XNUCLEO53L1A1_hi2c, Dev|1, pdata, count, i2c_time_out);
    if (status) {
        //VL6180x_ErrLog("I2C error 0x%x %d len", dev->I2cAddr, len);
        //XNUCLEO6180XA1_I2C1_Init(&hi2c1);
    }
    return status;
}

VL53L1_Error VL53L1_RdByte(uint16_t Dev, uint16_t index, uint8_t *data) {
    VL53L1_Error Status = VL53L1_ERROR_NONE;
    int32_t status_int;

	_I2CBuffer[0] = index>>8;
	_I2CBuffer[1] = index&0xFF;
    VL53L1_GetI2cBus();
    status_int = _I2CWrite(Dev, _I2CBuffer, 2);
    if( status_int ){
        Status = VL53L1_ERROR_CONTROL_INTERFACE;
        goto done;
    }
    status_int = _I2CRead(Dev, data, 1);
    if (status_int != 0) {
        Status = VL53L1_ERROR_CONTROL_INTERFACE;
    }
done:
    VL53L1_PutI2cBus();
    return Status;
}
```
-->